import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { ItemModel } from '../form-component/domain/item.model';
import { TableService } from './table-service';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'app-tables',
    templateUrl: './table.component.html',
    styleUrls: ['./table.component.scss'],
    animations: [routerTransition()]
})
export class TablesComponent implements OnInit {
    public items: ItemModel[];
    public bindId: number;
    public showAlert = false;
    searchText = ''
    constructor(private service: TableService, private modalService: NgbModal, private router: Router) {}

    ngOnInit() {
        this.get();
    }

    open(content, id) {
        this.bindId = id;
        this.modalService.open(content);
    }

    deleteItem() {
        this.service.delete(this.bindId).subscribe(res => {
            this.get();
            this.showAlert = true;
            setTimeout(() => {
                this.showAlert = false;
            }, 1500);
        });
    }

    get() {
        this.service.get().subscribe(res => {
            this.items = res;
        });
    }
}
