import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ItemModel } from '../form-component/domain/item.model';
import { TableProvider } from './table-provider';

@Injectable()
export class TableService {

  constructor(private provider: TableProvider) {
  }

  get(): Observable<ItemModel[]> {
    return this.provider.get();
  }

  delete(id: number) {
    return this.provider.delete(id);
  }
}