import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { ItemModel } from '../form-component/domain/item.model';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { catchError } from 'rxjs/operators';

@Injectable()
export class TableProvider {
    constructor(private http: HttpClient) {}

    public get(): Observable<ItemModel[]> {
        return this.http.get<ItemModel[]>('http://localhost:3000/items').pipe(catchError(this.handleError));
    }

    delete(id: number) {
        return this.http.delete(`http://localhost:3000/items/${id}`).pipe(catchError(this.handleError));
    }

    private handleError(error: HttpErrorResponse): Observable<never> {
        if (error.error instanceof ErrorEvent) {
            console.error('Erro:', error.error.message);
        } else {
            console.error(`Backend respondeu com o erro: ${error.status}, ` + `${error.error}`);
        }
        return throwError('Erro :((');
    }
}
