import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { TableProvider } from './table-provider';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { LOCALE_ID } from '@angular/core';
import { TablesRoutingModule } from './table-routing.module';
import { PageHeaderModule } from 'src/app/shared';
import { TablesComponent } from './table.component';
import { TableService } from './table-service';
import { FilterPipe } from 'src/app/shared/pipes/filter-pipe';
import { FormsModule } from '@angular/forms';

@NgModule({
    imports: [CommonModule, TablesRoutingModule, NgbModule, PageHeaderModule, FormsModule],
    declarations: [TablesComponent, FilterPipe],
    providers: [
        TableService,
        TableProvider,
        [{provide: LOCALE_ID, useValue: 'pt-BR'}]
    ],
})
export class TablesModule {}
