import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import createNumberMask from 'text-mask-addons/dist/createNumberMask';
import { NgbDate } from '@ng-bootstrap/ng-bootstrap';
import { ItemModel } from './domain/item.model';
import { FormComponentService } from './form-service.component';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';

@Component({
    selector: 'app-form',
    templateUrl: './form.component.html',
    styleUrls: ['./form.component.scss'],
    animations: [routerTransition()]
})
export class FormComponent implements OnInit {
    public form: FormGroup;
    public submitted = false;
    public item: ItemModel;
    public showAlert = false;

    public units = [
        {
            id: 1,
            name: 'Litro',
            short: 'lt',
            maxLength: 10,
            mask: {
                prefix: '',
                suffix: ' lt',
                allowDecimal: true,
                decimalSymbol: ',',
                guide: true,
                thousandsSeparatorSymbol: '.',
                requireDecimal: false,
                decimalLimit: 3
            }
        },
        {
            id: 2,
            name: 'Quilograma',
            short: 'kg',
            maxLength: 20,
            mask: {
                requireDecimal: false,
                suffix: ' kg',
                allowDecimal: true,
                decimalSymbol: ',',
                thousandsSeparatorSymbol: '.',
                prefix: '',
                decimalLimit: 3
            }
        },
        {
            id: 4,
            name: 'Unidade',
            short: 'un',
            maxLength: 40,
            mask: {
                prefix: '',
                suffix: ' un',
                allowDecimal: false,
                decimalSymbol: '',
                thousandsSeparatorSymbol: '',
                requireDecimal: false
            }
        }
    ];

    public selectedUnit = this.units[0];
    public customValidation = false;
    public isExpired = false;
    public dateMask = [/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/];
    public manufactureErr = false;
    public isEdit = false;

    constructor(private formBuilder: FormBuilder,
        private service: FormComponentService,
        private router: Router,
        private route: ActivatedRoute) {

        this.createFormRef();
    }

    ngOnInit() {
        this.route.params.subscribe(params => this.get(params));
    }

    public get(param) {
        if (!param.id) {
            return;
        }
        this.service.getById(param.id).subscribe(res => {
            this.isEdit = true;
            this.form.patchValue({
                ...res
            });
        });
    }

    public createFormRef() {
        this.form = this.formBuilder.group({
            id: null,
            itemName: ['', Validators.required],
            quantity: ['', Validators.required],
            price: '',
            perishable: [],
            expireDate: [{}, Validators.required],
            dateOfManufacture: [{}, Validators.required]
        });
    }

    get f() {
        return this.form.controls;
    }

    public onSubmit() {
        this.submitted = true;
        if (this.form.invalid) {
            return;
        }

        this.service.save(this.form.value).subscribe(res => {
            this.showAlert = true;
            setTimeout(() => {
                this.router.navigate(['table']);
            }, 1000);

        });
    }

    public getMask(): Array<RegExp | String> {
        return createNumberMask(this.selectedUnit.mask);
    }

    public onChange(id) {
        this.selectedUnit = this.units.find(unit => {
            return unit.id === +id;
        });
    }

    public alphabeticOnly(event): boolean {
        const charCode = event.which ? event.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return true;
        }
        return false;
    }

    public setCustomValidation() {
        this.customValidation = !this.form.controls['perishable'].value;
    }

    public expireDateChecker(date: NgbDate) {
        const today = new Date();
        const converted = new Date(date.year, date.month - 1, date.day);
        if (this.form.controls['expireDate'].value) {
            this.dateOfManufactureChecker(this.form.controls['expireDate'].value);
        }

        if (converted.getTime() < today.getTime()) {
            this.isExpired = true;
            this.form.controls['expireDate'].setErrors({ errors: true });
        }
    }

    public dateOfManufactureChecker(date: NgbDate) {
        const converted = new Date(`${date.year}-${date.month - 1}-${date.day}`);
        const check = this.form.controls['expireDate'].value;
        const toDate = new Date(`${check.year}-${check.month - 1}-${check.day}`);

        if (toDate < converted) {
            this.form.controls['dateOfManufacture'].setErrors({ errors: true });
            this.manufactureErr = true;
        }
    }
}
