import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ItemModel } from '../form-component/domain/item.model';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class FormProvider {
  constructor(private http: HttpClient ) {
  }

  get(id: string): Observable<ItemModel> {
    return this.http.get<ItemModel>(`http://localhost:3000/items/${id}`)
    .pipe(

    );

  }
  save(item: ItemModel) {
    return this.http.post<ItemModel>(`http://localhost:3000/items`, item)
    .pipe(

    );
  }

  edit(item: ItemModel) {
    return this.http.put<ItemModel>(`http://localhost:3000/items/${item.id}`, item).pipe(

    )

  }
}