import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormRoutingModule } from './form-routing.module';
import { FormComponent } from './form.component';
import { PageHeaderModule } from './../../shared';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TextMaskModule } from 'angular2-text-mask';
import { NgxCurrencyModule } from 'ngx-currency';
import { NgbDatepickerModule, NgbDatepickerI18n, NgbDateParserFormatter, NgbAlertModule, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CustomDatepickerI18n, I18n } from 'src/app/shared/services/date-adapter.service';
import { NgbDatePTParserFormatter } from 'src/app/shared/services/date-parser.formatter';
import { FormComponentService } from './form-service.component';
import { FormProvider } from './form-provider';

@NgModule({
    imports: [
        CommonModule,
        FormRoutingModule,
        PageHeaderModule,
        FormsModule,
        ReactiveFormsModule,
        TextMaskModule,
        NgxCurrencyModule,
        NgbDatepickerModule,
        NgbAlertModule,
        NgbModule
    ],
    declarations: [FormComponent],
    providers: [
        [I18n, { provide: NgbDatepickerI18n, useClass: CustomDatepickerI18n }],
        [{provide: NgbDateParserFormatter, useClass: NgbDatePTParserFormatter}],
        FormComponentService,
        FormProvider
    ]
})
export class FormModule {}
