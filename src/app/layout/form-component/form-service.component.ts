import { Injectable } from '@angular/core';
import { ItemModel } from './domain/item.model';
import { Observable } from 'rxjs';
import { FormProvider } from './form-provider';

@Injectable()
export class FormComponentService {

  constructor(private provider: FormProvider) {
  }

  save(item: ItemModel) {
      if (!item.id) {
        return this.provider.save(item);
      }
      return this.provider.edit(item);
  }

  public getById(id: string): Observable<ItemModel> {
    return this.provider.get(id);
  }
}