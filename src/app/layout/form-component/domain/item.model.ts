export class ItemModel {
  id: number;
  itemName: string;
  unitOfMeasurement: string;
  quantity: string;
  price: string;
  perishable: boolean;
  expireDate: string;
  dateOfManufacture: string;
}