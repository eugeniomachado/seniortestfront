import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout.component';

const routes: Routes = [
    {
        path: '',
        component: LayoutComponent,
        children: [
            { path: '', redirectTo: 'table', pathMatch: 'prefix' },
            { path: 'table', loadChildren: () => import('./table/table.module').then(m => m.TablesModule) },
            { path: 'forms', loadChildren: () => import('./form-component/form.module').then(m => m.FormModule) },
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LayoutRoutingModule {}
